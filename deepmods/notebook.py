
# ==================================================================
#            _   _       _       _                 _    
#            | \ | |     | |     | |               | |   
#            |  \| | ___ | |_ ___| |__   ___   ___ | | __
#            | . ` |/ _ \| __/ _ \ '_ \ / _ \ / _ \| |/ /
#            | |\  | (_) | ||  __/ |_) | (_) | (_) |   < 
#            |_| \_|\___/ \__\___|_.__/ \___/ \___/|_|\_\
#                                                   module                                       
# ==================================================================
# A simple module to host some common functions
#

import os
import glob
from datetime import datetime
import itertools
import datetime
import logging

import math
import numpy as np

import tensorflow as tf
from tensorflow import keras

import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

VERSION='0.4.1'

# ---- misc
#
_run_id    = ''
_save_figs = False

# ---- Folders generic name
#
_model_dir   =   './run/lab-{}/model'
_check_dir   =   './run/lab-{}/check'
_log_dir     =   './run/lab-{}/log'
_fig_dir     =   './run/lab-{}/fig'
_run_dir     =   './run/lab-{}'


# -------------------------------------------------------------
# init_all
# -------------------------------------------------------------
#
def init(id='tmp', cleanit=False, save_figs=True, hide_warning=True, mplstyle='deepmods/talk.mplstyle'):
    global _model_dir,_check_dir,_log_dir,_fig_dir, _run_dir
    global _run_id, _save_figs, VERSION

    # ---- id
    _run_id=id

    # ---- Complete dirnames
    _run_dir   = _run_dir.format(_run_id)
    _model_dir = _model_dir.format(_run_id)
    _check_dir = _check_dir.format(_run_id)
    _log_dir   = _log_dir.format(_run_id)
    _fig_dir   = _fig_dir.format(_run_id)
    
    # ---- Folders initialisation
    init_folder(_model_dir)
    init_folder(_check_dir)
    init_folder(_log_dir)
    init_folder(_fig_dir)

    # ---- matplotlib
    matplotlib.style.use(mplstyle)
    
    # ---- Hello world
    now = datetime.datetime.now()
    print('\nDeepmod by pjluc 2019')
    print('  Version          :', VERSION)
    print('  Run time         : {}'.format(now.strftime("%A %-d %B %Y, %H:%M:%S")))
    print('  Run directory    :', _run_dir)
    print('  Save figs        :', save_figs)
    print('  Matplotlib style :', mplstyle)
    print('  Hide warning     :', hide_warning)
    #
    print('\nTensorFlow version : ',tf.__version__)
    print('Keras version      : ',tf.keras.__version__)

    # ---- Figs
    _save_figs=save_figs
    
    # ---- Warnings
    logging.getLogger('tensorflow').disabled = True
    print("\nInit done.")
          
# -------------------------------------------------------------
# init_folder
# -------------------------------------------------------------
#
def init_folder(path, cleanit=False, about=''):
#     print('{:20s} {:30s}'.format(about,path), end='')
    os.makedirs(path, mode=0o750, exist_ok=True)
#     print('(created)',end='')
#     if cleanit:
#         files = glob.glob(path+'/events.out.*')
#         for f in files:
#             os.remove(f)
#         print('(cleaned)',end='')
#     print()
      
        
def get_log_dir():
    global _log_dir
    now = datetime.datetime.now()
    path='{}/{}'.format(_log_dir, now.strftime("%Y-%m-%d_%Hh%Mm%Ss"))
    print('log dir is         : {}'.format(path))
    print('To run TensorBoard : # tensorboard --logdir="{}"'.format(_log_dir))
    return path

def get_model_dir(): 
    global _model_dir
    return _model_dir

def get_check_dir(): 
    global _check_dir
    return _check_dir

def get_fig_dir(): 
    global _fig_dir
    return _fig_dir

# -------------------------------------------------------------
# shuffle_dataset
# -------------------------------------------------------------
#
def shuffle_dataset(x, y):
    assert (len(x) == len(y)), "x and y must have same size"
    p = np.random.permutation(len(x))
    return x[p], y[p]

# -------------------------------------------------------------
# show_images
# -------------------------------------------------------------
#
def plot_images(x,y, indices, columns=12, x_size=1, y_size=1, colorbar=False, y_pred=None,save_as=None, cm='binary'):
    """
    Show some images in a grid, with legends
    args:
        X: images
        y: real classes
        indices: indices of images to show
        columns: number of columns (12)
        x_size,y_size: figure size
        colorbar: show colorbar (False)
        y_pred: predicted classes (None)
        cm: Matplotlib olor map
    returns: 
        nothing
    """
    rows    = math.ceil(len(indices)/columns)
    fig=plt.figure(figsize=(columns*x_size, rows*(y_size+0.35)))
    n=1
    errors=0 
    if np.any(y_pred)==None:
        y_pred=y
    for i in indices:
        axs=fig.add_subplot(rows, columns, n)
        n+=1
#         img=axs.imshow(x[i],cmap = cm, interpolation='lanczos')
        img=axs.imshow(x[i],cmap = cm)
        axs.spines['right'].set_visible(True)
        axs.spines['left'].set_visible(True)
        axs.spines['top'].set_visible(True)
        axs.spines['bottom'].set_visible(True)
        axs.set_yticks([])
        axs.set_xticks([])
        if y[i]!=y_pred[i]:
            axs.set_xlabel('{} ({})'.format(y_pred[i],y[i]))
            axs.xaxis.label.set_color('red')
            errors+=1
        else:
            axs.set_xlabel(y[i])
        if colorbar:
            fig.colorbar(img,orientation="vertical", shrink=0.65)
    if save_as!=None:
        save_fig(save_as, svg=False)
    plt.show()

    
# -------------------------------------------------------------
# show_digit3d
# -------------------------------------------------------------
#
def plot_image3d(Xi, x_size=1, y_size=1, cm='gray'):
    """
    Show a digit in 3d
    args:
        Xi: Image digit to display
        x_size,y_size: figure size
        cm: Matplotlib olor map
    returns: 
        nothing
    """
    fig = plt.figure(figsize=(x_size, y_size))
    axs = fig.gca(projection='3d')
    X = np.linspace(0, -27, 28)
    Y = np.linspace(0, 27, 28)
    X, Y = np.meshgrid(X, Y)
    Z = Xi
    axs.set_zlim3d(0,1)
    axs.set_xticks([])
    axs.set_yticks([])
    axs.set_zticks([])
    axs.plot_surface(X,Y,Z, cmap='gray',linewidth=0, antialiased=True)
    axs.view_init(60, 70)
    plt.show()
    

# -------------------------------------------------------------
# show_history
# -------------------------------------------------------------
#
def plot_history(history, figsize=(8,6), save_as=None):
    """
    Show history
    args:
        history: history
        save_as: filename to save or None
    """
    # Accuracy 
    plt.figure(figsize=figsize)
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    if save_as!=None:
        save_fig(save_as+'-acc', svg=False)
    plt.show()

    # Loss values
    plt.figure(figsize=figsize)
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    if save_as!=None:
        save_fig(save_as+'-loss', svg=False)
    plt.show()    


def plot_confusion_matrix(cm,
                          target_names,
                          title='Confusion matrix',
                          figsize=(8,6),
                          cmap=None,
                          normalize=True,
                          save_as=None):
    """
    given a sklearn confusion matrix (cm), make a nice plot

    Args:
        cm:           confusion matrix from sklearn.metrics.confusion_matrix
        target_names: given classification classes such as [0, 1, 2]
                      the class names, for example: ['high', 'medium', 'low']
        title:        the text to display at the top of the matrix
        cmap:         color map
        normalize:    False : plot raw numbers, True: plot proportions
        save_as:      If not None, filename to save

    Citiation:
        http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html

    """
 
    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    if (figsize[0]==figsize[1]):
        aspect='equal'
    else:
        aspect='auto'

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=figsize)
    plt.imshow(cm, interpolation='nearest', cmap=cmap, aspect=aspect)
    plt.title(title)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]


    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, "{:0.4f}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")


    # plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))

    if save_as!=None:
        save_fig(save_as, svg=False)

    plt.show()

    
# -------------------------------------------------------------
# save_fig
# -------------------------------------------------------------
#
def save_fig(filename, png=True, svg=False):
    global _fig_dir, _run_id, _save_figs
#     if _save_figs and png : plt.savefig('{}/{}-{}.png'.format(_fig_dir, _run_id,filename))
#     if _save_figs and svg : plt.savefig('{}/{}-{}.svg'.format(_fig_dir, _run_id,filename))            
    if _save_figs and png : plt.savefig( get_fig_name(filename,'png') )
    if _save_figs and svg : plt.savefig( get_fig_name(filename,'svg') )           

def get_fig_name(filename,ext):
    global _fig_dir, _run_id, _save_figs
    return '{}/{}-{}.{}'.format(_fig_dir, _run_id,filename, ext)