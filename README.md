

Notebooks of "Deep Learning" talk
=================================

by Jean-Luc Parouty, CNRS/SIMaP 2019

This repository contains most of the **Jupyter Lab notebooks** used in my presentation:

[![image](talks/ai.png)](./talks/Presentation.pdf)

You can [get the pdf here !](./talks/Presentation.pdf)

## About

These notebooks and documents :
- are freely reusable (MIT licenses)
- require few resources (basic laptop)
- have been inspired by thousands of similar examples, particularly in Aurélien Géron's excellent book "Hands-On Machine Learning with Scikit-Learn & TensorFlow", translated into French by Hervé Soulard: "Deep Learning avec TensorFlow", available from Dunod.

## How to get and use Notebooks ?

### Option 1 : With Binder 

You don't have to do anything!
Just click on the Binder button below

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Ftalks%2Fdeeplearning.git/master?urlpath=lab/tree/index.ipynb)

Binder limitatons :
- The launch can sometimes take a little time
- Examples 8 and 9 need a basic local web server, so they cannot be used directly with Binder.
- Some notebooks are resource-intensive and may not work properly in Binder's environment.

### Option 2 : Clone and run it !

It will take about 10'  
Works on Windows, Linux or MacOS

#### Prerequisites
- Jupyter lab, For example with [Anaconda](https://www.anaconda.com), with python >=3.5  

#### Get it and install
Clone notebooks :  
`# git clone git@gricad-gitlab.univ-grenoble-alpes.fr:talks/deeplearning.git`

Create conda environment ("deeplearning") :  
`# conda env create -f environment.yml`

#### Start and play

Activate environnement :  
`# conda activate deeplearning`  

Run jupyter lab :  
 `# jupyter lab`  

Open **index.ipynb** notebook 

Have fun !
